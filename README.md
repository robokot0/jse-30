# Task Manager
## Software requirements
|Software|Version|
|----------------|---------|
|Java|openjdk version "11"|
|Apache Maven|Apache Maven 3.6.1|
|OS|Windows 8.1|
## Description of the technology stack
Apache Maven 3
## Contacts
|Name|e-mail|
|----------------|---------|
|Korshunov Andrey|robokot0@gmail.com|
## Commands for building the application
|Command|Result|
|----------------|---------|
|mvn clean|Clearing the project directory|
|mvn package|Creating executable files|
## Application Launch Commands
|Command|Command line|Description|
|----------------|---------|---------|
|version|java -jar ./task-manager-1.0.0.jar `command`|Execute `command`|
|command line|java -jar ./task-manager-1.0.0.jar|Display line to enter command|
## Application Commands
|Command|CDescription|Short command|
|----------------|---------|---------|
|version|Display version|v|
|about|Display developer info|a|
|help|Display list of command|h|
|exit|Terminare program|e|
|save-to-json|Save data to JSON|sj|");
|save-to-xml|Save data to XML|sx|");
|load-test-data|Load test data|ltd|");
|command-history-view|View command history|chv|
|project-create|Create new project by name.|pcr|
|project-clear|Remove all projects.|pcl|
|project-list|Display list of projects.|pl|
|project-view|View project info.|pv|
|project-remove-by-id|Remove project by id.|prid|
|project-remove-by-name|Remove project by name.|prn|
|project-remove-by-index|Remove project by index.|prin|
|project-update-by-index|Update name and description of project by index.|puin|
|task-create|Create new task by name.|tcr|
|task-clear|Remove all tasks.|tcl|
|task-list|Display list of tasks.|tl|
|task-view|View task info.|tv|
|task-remove-by-id|Remove task by id.|trid|
|task-remove-by-name|Remove task by name.|trn|
|task-remove-by-index|Remove task by index.|trin|
|task-update-by-index|Update name and description of task by index.|tuin|
|task-list-by-project-id|Display task list by project if.|tlp|
|task-add-to-project-by-ids|Add task to project by id.|tap|
|task-remove-from-project-by-ids|Remove task from project by id|trp|
|user-create|Create user.|uc|
|user-list|View list of users.|ul|
|user-view-by-id|View user data by id.|uvid|
|user-view-by-index|View user data by index.|uvi|
|user-view-by-login|View user data by login.|uvl|
|user-remove-by-id|Remove user by id.|urid|
|user-remove-by-index|Remove user by index.|uri|
|user-remove-by-login|Remove user by login.|url|
|user-update-by-id|Update user data by id.|uuid|
|user-update-by-index|Update user data by index.|uui|
|user-update-by-login|Update user data by login.|uul|
|user-update-password-by-id|Change password of user by id.|uupid|
|user-update-password-by-index|Change password of user by index.|uupi|
|user-update-password-by-login|Change password of user by login.|uupl|
|user-auth|Auth user.|ua|
|user-update-password|Update password of auth user.|uup|
|user-view|View auth user data.|uv|
|user-update|Update auth user data|uu|
|user-end-session|End auth user session.|ue|
|user-of-project-set-by-index|Set user of project. Project find by index|up|
|user-of-task-set-by-index|Set user of task. Task find by index.|ut|
## GIT
<https://gitlab.com/robokot0/jse-30>  

