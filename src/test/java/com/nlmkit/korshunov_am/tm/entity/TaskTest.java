package com.nlmkit.korshunov_am.tm.entity;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TaskTest {

    @Test
    public void getIdsetId() {
        Task task = new Task();
        task.setId(Long.MAX_VALUE);
        assertEquals(task.getId().longValue(),Long.MAX_VALUE);
    }

    @Test
    public void getNamesetName() {
        Task task = new Task();
        task.setName("aaa");
        assertEquals(task.getName(),"aaa");
    }

    @Test
    public void getDescriptionsetDescription() {
        Task task = new Task();
        task.setDescription("aaaa");
        assertEquals(task.getDescription(),"aaaa");
    }

    @Test
    public void getProjectIdsetProjectId() {
        Task task = new Task();
        task.setProjectId(Long.MAX_VALUE);
        assertEquals(task.getProjectId().longValue(),Long.MAX_VALUE);
    }

    @Test
    public void getUserIdsetUserId() {
        Task task = new Task();
        task.setUserId(Long.MAX_VALUE);
        assertEquals(task.getUserId().longValue(),Long.MAX_VALUE);
    }

    @Test
    public void testToString() {
        Task task = new Task("aaa",Long.MAX_VALUE);
        assertEquals(task.toString(),task.getId() + ": " + task.getName());
    }
}