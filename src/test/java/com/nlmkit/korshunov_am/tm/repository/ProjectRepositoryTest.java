package com.nlmkit.korshunov_am.tm.repository;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.entity.Project;

import java.io.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class ProjectRepositoryTest {
    static ProjectRepository projectRepository = ProjectRepository.getInstance();
    @BeforeAll
    public static void SetUp() throws IOException {
        projectRepository = ProjectRepository.getInstance();

        projectRepository.create("P2","D1",1L);
        projectRepository.create("P1","D2",1L);

        ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();
        projectRepository.saveAs(new JsonMapper(),byteArrayOutputStream1);
        projectRepository.clear();
        ByteArrayInputStream byteArrayInputStream1 = new ByteArrayInputStream(byteArrayOutputStream1.toByteArray());
        projectRepository.loadFrom(new JsonMapper(),byteArrayInputStream1);

        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        projectRepository.saveAs(new XmlMapper(),byteArrayOutputStream2);
        projectRepository.clear();
        ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(byteArrayOutputStream2.toByteArray());
        projectRepository.loadFrom(new XmlMapper(),byteArrayInputStream2);

        projectRepository.clear();
    }

    @Test
    public void createByNameUserId() {
        Project project1 = projectRepository.create("aaa", Long.MAX_VALUE);
        assertEquals(project1.getId().longValue(), projectRepository.findById(project1.getId()).getId().longValue());
        assertEquals(project1.getName(), projectRepository.findById(project1.getId()).getName());
    }
    @Test
    public void createByNameDescriptionUserId() {
        Project project2 = projectRepository.create("aaa", "bbb", Long.MIN_VALUE);
        assertEquals(project2.getId().longValue(), projectRepository.findById(project2.getId()).getId().longValue());
        assertEquals(projectRepository.findById(project2.getId()).getName(), "aaa");
        assertEquals(projectRepository.findById(project2.getId()).getDescription(), "bbb");
    }

    @Test
    public void update() {
        Project project = projectRepository.create("aaa1", "bbb1", Long.MAX_VALUE);
        projectRepository.update(project.getId(), "aaa2", "bbb2", Long.MAX_VALUE);
        assertEquals(projectRepository.findById(project.getId()).getName(), "aaa2");
        assertEquals(projectRepository.findById(project.getId()).getDescription(), "bbb2");
    }

    @Test
    public void testClear() {
        projectRepository.create("aaa", Long.MAX_VALUE);
        projectRepository.create("aaa", Long.MIN_VALUE);
        assertNotEquals(projectRepository.size(Long.MAX_VALUE), 0);
        assertNotEquals(projectRepository.size(Long.MIN_VALUE), 0);
        projectRepository.clear(Long.MAX_VALUE);
        assertEquals(projectRepository.size(Long.MAX_VALUE), 0);
        assertNotEquals(projectRepository.size(Long.MIN_VALUE), 0);
    }

    @Test
    public void findByIndex() {
        projectRepository.clear();
        projectRepository.create("aaa02", Long.MAX_VALUE);
        projectRepository.create("aaa01", Long.MIN_VALUE);
        assertEquals(projectRepository.findByIndex(0).getName(), "aaa01");
    }
    @Test
    public void findByIndexNotFound() {
        projectRepository.clear();
        projectRepository.create("aaa02", Long.MAX_VALUE);
        projectRepository.create("aaa01", Long.MIN_VALUE);
        assertNull(projectRepository.findByIndex(155));
    }

    @Test
    public void testFindByIndex() {
        projectRepository.clear();
        projectRepository.create("aaa003", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa000", "bbb3", Long.MAX_VALUE);
        assertEquals(projectRepository.findByIndex(0, Long.MAX_VALUE).getName(), "aaa000");
        assertEquals(projectRepository.findByIndex(0, Long.MIN_VALUE).getName(), "aaa003");
    }
    @Test
    public void testFindByIndexNull() {
        projectRepository.clear();
        projectRepository.create("aaa003", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa000", "bbb3", Long.MAX_VALUE);
        assertNull(projectRepository.findByIndex(9999, Long.MAX_VALUE));
        assertNull(projectRepository.findByIndex(88880, Long.MIN_VALUE));
    }

    @Test
    public void findByName() {
        projectRepository.create("aaa3", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa0", "bbb3", Long.MAX_VALUE);
        assertEquals(projectRepository.findByName("aaa3").getName(), "aaa3");
    }

    @Test
    public void findByNameNull() {
        assertNull(projectRepository.findByName("aaa54654563"));
    }

    @Test
    public void testFindByName() {
        projectRepository.create("aaa3", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa0", "bbb3", Long.MAX_VALUE);
        assertEquals(projectRepository.findByName("aaa3", Long.MIN_VALUE).getName(), "aaa3");
        assertEquals(projectRepository.findByName("aaa0", Long.MAX_VALUE).getName(), "aaa0");
    }

    @Test
    public void testFindByNameNull() {
        assertNull(projectRepository.findByName("aaa3464655", Long.MIN_VALUE));
        assertNull(projectRepository.findByName("aaa46546540", Long.MAX_VALUE));
    }

    @Test
    public void findById() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("aaa3","bbb3",Long.MIN_VALUE);
        assertEquals(projectRepository.findById(project.getId()).getId(),project.getId());
    }

    @Test
    public void findByIdNull() {
        assertNull(projectRepository.findById(6546546L));
    }

    @Test
    public void testFindById() {
        Project project = projectRepository.create("aaa3", "bbb3", Long.MIN_VALUE);
        Project project1 = projectRepository.create("aaa0", "bbb3", Long.MAX_VALUE);
        assertEquals(projectRepository.findById(project.getId(),Long.MIN_VALUE).getId(),project.getId());
        assertEquals(projectRepository.findById(project1.getId(),Long.MAX_VALUE).getId(),project1.getId());
    }

    @Test
    public void testFindByIdNull() {
        projectRepository.create("aaa3", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa0", "bbb3", Long.MAX_VALUE);
        assertNull(projectRepository.findById(4465456456L,Long.MIN_VALUE));
        assertNull(projectRepository.findById(4464645185L,Long.MAX_VALUE));
    }

    @Test
    public void removeByIndex() {
        projectRepository.create("333333333aaa3", "bbb3", Long.MIN_VALUE);
        projectRepository.create("000000000aaa0", "bbb3", Long.MAX_VALUE);
        projectRepository.removeByIndex(0);
        assertEquals(projectRepository.findByIndex(0).getName(),"333333333aaa3");
    }

    @Test
    public void testRemoveByIndex() {
        projectRepository.create("aaa3", "bbb3", 11L);
        projectRepository.create("aaa0", "bbb3", 11L);
        projectRepository.create("aaa3", "bbb3", 21L);
        projectRepository.create("aaa0", "bbb3", 21L);
        projectRepository.removeByIndex(0,11L);
        assertEquals(projectRepository.findByIndex(0,11L).getName(),"aaa3");
        assertEquals(projectRepository.findByIndex(0,21L).getName(),"aaa0");
    }

    @Test
    public void removeById() {
        projectRepository.clear();
        Project project = projectRepository.create("aaa3", "bbb3", Long.MIN_VALUE);
        assertNotEquals(projectRepository.size(),0);
        projectRepository.removeById(project.getId());
        assertEquals(projectRepository.size(),0);
    }

    @Test
    public void testRemoveById() {
        projectRepository.clear();
        Project project1 = projectRepository.create("aaa3", "bbb3", 1L);
        projectRepository.create("aaa0", "bbb3", 1L);
        projectRepository.create("aaa3", "bbb3", 2L);
        projectRepository.create("aaa0", "bbb3", 2L);
        assertEquals(projectRepository.size(1L),2);
        assertEquals(projectRepository.size(2L),2);
        projectRepository.removeById(project1.getId(),1L);
        assertEquals(projectRepository.size(1L),1);
        assertEquals(projectRepository.size(2L),2);
    }

    @Test
    public void removeByName() {
        projectRepository.clear();
        Project project1 = projectRepository.create("aaa1", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa2", "bbb3", Long.MIN_VALUE);
        assertEquals(projectRepository.size(),2);
        projectRepository.removeByName(project1.getName());
        assertEquals(projectRepository.size(),1);
    }

    @Test
    public void testRemoveByName() {
        projectRepository.clear();
        Project project1 = projectRepository.create("aaa1", "bbb3", 1L);
        projectRepository.create("aaa2", "bbb3", 1L);
        projectRepository.create("aaa3", "bbb3", 2L);
        projectRepository.create("aaa4", "bbb3", 2L);
        assertEquals(projectRepository.size(1L),2);
        assertEquals(projectRepository.size(2L),2);
        projectRepository.removeByName(project1.getName(),1L);
        assertEquals(projectRepository.size(1L),1);
        assertEquals(projectRepository.size(2L),2);
    }

    @Test
    public void findAll() {
        projectRepository.clear();
        projectRepository.create("aaa1", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa2", "bbb3", Long.MIN_VALUE);
        assertEquals(projectRepository.findAll().size(),2);
    }

    @Test
    public void testFindAll() {
        projectRepository.clear();
        projectRepository.create("aaa1", "bbb3", 1L);
        projectRepository.create("aaa2", "bbb3", 1L);
        projectRepository.create("aaa3", "bbb3", 2L);
        assertEquals(projectRepository.findAll(1L).size(),2);
        assertEquals(projectRepository.findAll(2L).size(),1);
    }

    @Test
    public void size() {
        projectRepository.clear();
        projectRepository.create("aaa1", "bbb3", Long.MIN_VALUE);
        projectRepository.create("aaa2", "bbb3", Long.MIN_VALUE);
        assertEquals(projectRepository.size(),2);
    }

    @Test
    public void testSize() {
        projectRepository.clear();
        projectRepository.create("aaa1", "bbb3", 1L);
        projectRepository.create("aaa2", "bbb3", 1L);
        projectRepository.create("aaa3", "bbb3", 2L);
        assertEquals(projectRepository.size(),3);
    }

    @Test
    public void getInstance() {
        assertEquals(projectRepository,ProjectRepository.getInstance());
    }

}