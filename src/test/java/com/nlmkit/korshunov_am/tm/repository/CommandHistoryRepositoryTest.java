package com.nlmkit.korshunov_am.tm.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CommandHistoryRepositoryTest {

    @BeforeAll
    public static void beforeTtestAddCommandsToHistory(){
        CommandHistoryRepository commandHistoryRepository = CommandHistoryRepository.getInstance();
        commandHistoryRepository.AddCommandToHistory("testcommand1");
        commandHistoryRepository.AddCommandToHistory("testcommand2");
        commandHistoryRepository.AddCommandToHistory("testcommand3");
        commandHistoryRepository.AddCommandToHistory("testcommand4");
        commandHistoryRepository.AddCommandParameterToLastCommand("testparameter","testparametervalue");
        commandHistoryRepository.AddCommandToHistory("testcommand5");
        commandHistoryRepository.AddCommandResultToLastCommand("testresult");
        commandHistoryRepository.AddCommandToHistory("testcommand6");
        commandHistoryRepository.AddCommandToHistory("testcommand7");
        commandHistoryRepository.AddCommandToHistory("testcommand8");
        commandHistoryRepository.AddCommandToHistory("testcommand9");
        commandHistoryRepository.AddCommandToHistory("testcommand10");
        commandHistoryRepository.AddCommandToHistory("testcommand11");
        commandHistoryRepository.AddCommandToHistory("testcommand12");
    }

    @Test
    public void testGetInstance() {
        assertEquals(CommandHistoryRepository.getInstance(),CommandHistoryRepository.getInstance());
    }


    @Test
    public void testAddCommandToHistory() {
        CommandHistoryRepository commandHistoryRepository = CommandHistoryRepository.getInstance();
        assertEquals(commandHistoryRepository.findAll().size(),10);
        assertEquals(commandHistoryRepository.findAll().get(0).getCommandText(),"testcommand3");
    }

    @Test
    public void testAddCommandParameterToLastCommand() {
        CommandHistoryRepository commandHistoryRepository = CommandHistoryRepository.getInstance();
        assertEquals(commandHistoryRepository.findAll().get(1).getCommandText(),"testcommand4 testparameter testparametervalue");
    }

    @Test
    public void testAddCommandResultToLastCommand() {
        CommandHistoryRepository commandHistoryRepository = CommandHistoryRepository.getInstance();
        assertEquals(commandHistoryRepository.findAll().get(2).getCommandText(),"testcommand5 testresult");
    }

    @Test
    public void testFindAll() {
        CommandHistoryRepository commandHistoryRepository = CommandHistoryRepository.getInstance();
        assertEquals(commandHistoryRepository.findAll().size(),10);
        assertEquals(commandHistoryRepository.findAll().get(0).getCommandText(),"testcommand3");
        assertEquals(commandHistoryRepository.findAll().get(9).getCommandText(),"testcommand12");
    }
}