package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProjectListenerTest {
    static ProjectListener projectListener;
    static ProjectService projectService;
    static InputConsole inputConsole;
    static CommandHistoryService commandHistoryService;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        projectService = Mockito.mock(ProjectService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        projectListener = new ProjectListener(projectService,commandHistoryService);
        projectListener.setConsole(inputConsole);
    }

    @Test
    void setUsergetUser() {
        User user = new User("login");
        projectListener.setUser(user);
        assertEquals(user,projectListener.getUser());
    }

    @Test
    void enterStringCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputString("pn",false);
            projectListener.enterStringCommandParameter("pn");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn","pv");
        });
    }

    @Test
    void enterPasswordCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputPassword("pn1",false);
            projectListener.enterPasswordCommandParameter("pn1");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn1","******");
        });
    }

    @Test
    void enterIntegerCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn2",true);
            assertEquals(1,projectListener.enterIntegerCommandParameter("pn2"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn2","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn3",true);
            assertEquals(1,projectListener.enterIntegerCommandParameter("pn3"));
        });
    }

    @Test
    void enterLongCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn4",true);
            assertEquals(1L,projectListener.enterLongCommandParameter("pn4"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn4","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn5",true);
            assertEquals(1L,projectListener.enterLongCommandParameter("pn5"));
        });
    }

    @Test
    void showResult() {
        assertDoesNotThrow(()-> {
            projectListener.ShowResult("result");
            verify(inputConsole,times(1)).showMessage("result");
            verify(commandHistoryService,times(1)).AddCommandResultToLastCommand("result");
        });
    }

    @Test
    void testAuthUser() {
        projectListener.setUser(new User("testAuthUser"));
        assertTrue(projectListener.testAuthUser());
        projectListener.setUser(null);
        assertFalse(projectListener.testAuthUser());
    }

    @Test
    void testAdminUser() {
        User user = new User("testAdminUser");
        user.setRole(Role.ADMIN);
        projectListener.setUser(user);
        assertTrue(projectListener.testAdminUser());
        user.setRole(Role.USER);
        assertFalse(projectListener.testAdminUser());
        projectListener.setUser(null);
        assertFalse(projectListener.testAdminUser());
    }

    @Test
    void getInstance() {
        ProjectListener projectListener = ProjectListener.getInstance();
        assertEquals(projectListener,ProjectListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,projectListener.notify("aaaaaa"));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_CREATE));
        assertEquals(0,projectListener.notify(PROJECT_CREATE));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_CLEAR));
        assertEquals(0,projectListener.notify(PROJECT_CLEAR));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_LIST));
        assertEquals(0,projectListener.notify(PROJECT_LIST));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_VIEW));
        assertEquals(0,projectListener.notify(PROJECT_VIEW));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_REMOVE_BY_ID));
        assertEquals(0,projectListener.notify(PROJECT_REMOVE_BY_ID));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_REMOVE_BY_NAME));
        assertEquals(0,projectListener.notify(PROJECT_REMOVE_BY_NAME));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_REMOVE_BY_INDEX));
        assertEquals(0,projectListener.notify(PROJECT_REMOVE_BY_INDEX));
        assertEquals(0,projectListener.notify(SHORT_PROJECT_UPDATE_BY_INDEX));
        assertEquals(0,projectListener.notify(PROJECT_UPDATE_BY_INDEX));
        assertEquals(0,projectListener.notify(SHORT_SAVE_TO_XML));
        assertEquals(0,projectListener.notify(SAVE_TO_XML));
        assertEquals(0,projectListener.notify(SHORT_SAVE_TO_JSON));
        assertEquals(0,projectListener.notify(SAVE_TO_JSON));
        assertEquals(0,projectListener.notify(SHORT_LOAD_FROM_XML));
        assertEquals(0,projectListener.notify(LOAD_FROM_XML));
        assertEquals(0,projectListener.notify(SHORT_LOAD_FROM_XML));
        assertEquals(0,projectListener.notify(LOAD_FROM_JSON));
        assertEquals(0,projectListener.notify(SHORT_HELP));
        assertEquals(0,projectListener.notify(HELP));

        assertDoesNotThrow(()-> {
            Mockito.doThrow(IOException.class).when(projectService).loadFrom(any(),any());
            projectListener.notify(LOAD_FROM_JSON);
        });

    }

    @Test
    void createProject() {
        projectListener.setUser(new User("user"));
        assertDoesNotThrow(()-> {
            doReturn("pn").when(inputConsole).getNextInputString("project name",false);
            doReturn("pd").when(inputConsole).getNextInputString("project description",false);
            assertEquals(0, projectListener.createProject());
            verify(projectService,times(1)).create("pn","pd",projectListener.getUser().getId());
        });
    }

    @Test
    void updateProjectByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);

        Project project = new Project();
        project.setUserId(user.getId());
        project.setId(0L);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            doReturn(project).when(projectService).findByIndex(0,user.getId(),true);
            projectListener.updateProjectByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            doReturn(project).when(projectService).findByIndex(0,true);
            projectListener.updateProjectByIndex();
        });
    }

    @Test
    void removeProjectByName() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);

        assertDoesNotThrow(()->{
            doReturn("pn").when(inputConsole).getNextInputString("project name",false);
            projectListener.removeProjectByName();

            user.setRole(Role.ADMIN);
            doReturn("pn").when(inputConsole).getNextInputString("project name",false);
            projectListener.removeProjectByName();
        });
    }

    @Test
    void removeProjectByID() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            projectListener.removeProjectByID();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            projectListener.removeProjectByID();
        });
    }

    @Test
    void removeProjectByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            projectListener.removeProjectByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            projectListener.removeProjectByIndex();
        });
    }

    @Test
    void clearProject() {
        User user = new User("user");
        user.setRole(Role.USER);

        assertDoesNotThrow(()->{
            projectListener.setUser(null);
            projectListener.clearProject();

            projectListener.setUser(user);
            projectListener.clearProject();

            user.setRole(Role.ADMIN);
            projectListener.clearProject();
        });
    }

    @Test
    void listProject() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);
        List<Project> projects = new ArrayList<>();
        projects.add(new Project());
        assertDoesNotThrow(()->{
            doReturn(projects).when(projectService).findAll();
            projectListener.listProject();

            user.setRole(Role.ADMIN);
            projectListener.listProject();
        });
    }

    @Test
    void viewProjectByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);

        Project project = new Project();
        project.setUserId(user.getId());
        project.setId(0L);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            doReturn(project).when(projectService).findByIndex(0,user.getId(),true);
            projectListener.viewProjectByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            doReturn(project).when(projectService).findByIndex(0,true);
            projectListener.viewProjectByIndex();
        });
    }

    @Test
    void setProjectUserById() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);
        user.setRole(Role.ADMIN);

        Project project = new Project();
        project.setUserId(user.getId());
        project.setId(0L);
        project.setName("pn");
        project.setDescription("pd");

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn(project).when(projectService).findById(1L,true);
            projectListener.setProjectUserById(user);
        });
    }

    @Test
    void setProjectUserByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        projectListener.setUser(user);
        user.setRole(Role.ADMIN);

        Project project = new Project();
        project.setUserId(user.getId());
        project.setId(0L);
        project.setName("pn");
        project.setDescription("pd");

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project index",true);
            doReturn(project).when(projectService).findByIndex(0,true);
            projectListener.setProjectUserByIndex(user);
        });
    }

}