package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;

public class StringArrayConsoleImpl implements InputConsole{
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Массив для обработки
     */
    ArrayList<String> commandList = new ArrayList<>();
    /**
     * Приватный конструктор по умолчанию
     */
    public StringArrayConsoleImpl(String[] args){
        super();
        if (args != null)
            if (args.length >=1) {
                commandList.addAll(Arrays.asList(args));
            }
    }
    /**
     * Получить строку
     *
     * @param parameterName сообщение с пояснением
     * @param throwEmpty    exception еслине задано
     * @return Введенное значение
     * @throws WrongArgumentException ошибочное значение параметра
     */
    @Override
    public String getNextInputString(String parameterName, Boolean throwEmpty) throws WrongArgumentException {
        showMessage("Please enter "+parameterName+": ");
        if(commandList.size()==0) {
            if(throwEmpty) {
                throw new WrongArgumentException("End " + parameterName + " list");
            } else {
                return null;
            }
        }
        return commandList.remove(0);
    }

    /**
     * Получить Integer
     *
     * @param parameterName сообщение с пояснением
     * @param throwEmpty    exception еслине задано
     * @return Введенное значение преобразованное к Integer
     * @throws WrongArgumentException ошибочное значение параметра
     */
    @Override
    public Integer getNextInputInteger(String parameterName, Boolean throwEmpty) throws WrongArgumentException {
        String result;
        try {
            result = getNextInputString(parameterName, throwEmpty);
            if(result == null){
                return null;
            }
            return Integer.parseInt(result);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get Integer " + parameterName + " from " + parameterName);
        }
    }

    /**
     * Получить Long
     *
     * @param parameterName сообщение с пояснением
     * @param throwEmpty    exception еслине задано
     * @return Введенное значение преобразованное к Long
     * @throws WrongArgumentException ошибочное значение параметра
     */
    @Override
    public Long getNextInputLong(String parameterName, Boolean throwEmpty) throws WrongArgumentException {
        String result;
        try {
            result = getNextInputString(parameterName,throwEmpty);
            if(result == null){
                return null;
            }
            return Long.parseLong(result);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get Long " + parameterName + " from " + parameterName);
        }
    }

    /**
     * Получить пароль
     *
     * @param parameterName сообщение с пояснением
     * @param throwEmpty    exception еслине задано
     * @return Введенное значение
     * @throws WrongArgumentException ошибочное значение параметра
     */
    @Override
    public String getNextInputPassword(String parameterName, Boolean throwEmpty) throws WrongArgumentException {
        return getNextInputString(parameterName,throwEmpty);
    }

    @Override
    public void showMessage(String message) {
        logger.info(message);
    }
}
