package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;

public interface InputConsole {
    /**
     * Получить строку
     * @param parameterName сообщение с пояснением
     * @param throwEmpty exception еслине задано
     * @return Введенное значение
     */
    String getNextInputString(String parameterName,Boolean throwEmpty) throws WrongArgumentException;

    /**
     * Получить Integer
     * @param parameterName сообщение с пояснением
     * @param throwEmpty exception еслине задано
     * @return Введенное значение преобразованное к Integer
     * @throws WrongArgumentException Неправильнрый аргумент
     */
    Integer getNextInputInteger(String parameterName,Boolean throwEmpty) throws WrongArgumentException;
    /**
     * Получить Long
     * @param parameterName сообщение с пояснением
     * @param throwEmpty exception еслине задано
     * @return Введенное значение преобразованное к Long
     * @throws WrongArgumentException Неправильнрый аргумент
     */
    Long getNextInputLong(String parameterName,Boolean throwEmpty) throws WrongArgumentException;
    /**
     * Получить пароль
     * @param parameterName сообщение с пояснением
     * @param throwEmpty exception еслине задано
     * @return Введенное значение
     * @throws WrongArgumentException Неправильнрый аргумент
     */
    String getNextInputPassword(String parameterName,Boolean throwEmpty) throws WrongArgumentException;
    void showMessage(String message);
}
