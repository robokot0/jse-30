package com.nlmkit.korshunov_am.tm.exceptions;

/**
 * Исключение не найдена задача
 */
public class TaskNotFoundException extends MessageException {
    /**
     * Конструктор не найдена задача по параметру
     * @param name  имя параметра
     * @param value значение параметра
     */
    public TaskNotFoundException(String name,String value) {
        super("Не найдена задача по "+name+"="+value);
    }
    /**
     * Конструктор не найдена задача в проекте по параметру
     * @param name  имя параметра
     * @param value значение параметра
     */
    public TaskNotFoundException(String name,String value,Long projectId) {
        super("Не найдена задача в проекте "+projectId.toString()+" по "+name+"="+value);
    }
    /**
     * Конструктор не найдена задача пользователя по параметру
     * @param name имя параметра
     * @param userId ид пользователя
     * @param value значение параметра
     */
    public TaskNotFoundException(String name,final Long userId,String value) {
        super("Не найдена задача пользователя "+userId.toString()+" по "+name+"="+value);
    }
    /**
     * Конструктор не найдена задача пользователя в проекте по параметру
     * @param name имя параметра
     * @param userId ид пользователя
     * @param value значение параметра
     */
    public TaskNotFoundException(String name,final Long userId,String value,Long projectId) {
        super("Не найдена задача пользователя "+userId.toString()+" в проекте "+projectId.toString()+" по "+name+"="+value);
    }

}
