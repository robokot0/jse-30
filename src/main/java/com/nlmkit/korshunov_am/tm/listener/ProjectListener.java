package com.nlmkit.korshunov_am.tm.listener;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectService;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;

public class ProjectListener  extends AbstractListener {
    ProjectService projectService;
    /**
     * конструктор для тестирования
     */
    ProjectListener(ProjectService projectService, CommandHistoryService commandHistoryService){
        super(commandHistoryService);
        this.projectService=projectService;
    }
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectListener(){
        super(CommandHistoryService.getInstance());
        projectService = ProjectService.getInstance();
    }
    /**
     * Единственный экземпляр объекта ProjectListener
     */
    private static ProjectListener instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectListener
     * @return единственный экземпляр объекта ProjectListener
     */
    public static ProjectListener getInstance(){
        if (instance == null){
            instance = new ProjectListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_PROJECT_CREATE:
                case PROJECT_CREATE: return createProject();
                case SHORT_PROJECT_CLEAR:
                case PROJECT_CLEAR: return clearProject();
                case SHORT_PROJECT_LIST:
                case PROJECT_LIST: return listProject();
                case SHORT_PROJECT_VIEW:
                case PROJECT_VIEW: return viewProjectByIndex();
                case SHORT_PROJECT_REMOVE_BY_ID:
                case PROJECT_REMOVE_BY_ID: return removeProjectByID();
                case SHORT_PROJECT_REMOVE_BY_NAME:
                case PROJECT_REMOVE_BY_NAME: return removeProjectByName();
                case SHORT_PROJECT_REMOVE_BY_INDEX:
                case PROJECT_REMOVE_BY_INDEX: return removeProjectByIndex();
                case SHORT_PROJECT_UPDATE_BY_INDEX:
                case PROJECT_UPDATE_BY_INDEX: return updateProjectByIndex();
                case SHORT_SAVE_TO_XML:
                case SAVE_TO_XML: return saveAsXML();
                case SHORT_SAVE_TO_JSON:
                case SAVE_TO_JSON: return saveAsJson();
                case SHORT_LOAD_FROM_XML:
                case LOAD_FROM_XML: return loadFromXML();
                case SHORT_LOAD_FROM_JSON:
                case LOAD_FROM_JSON: return loadFromJSON();
                case SHORT_HELP:
                case HELP: return displayHelp();
                default:return -1;
            }
        }
        catch (MessageException | IOException e) {
            ShowResult("[FAIL] "+e.getMessage());
            }
        return 0;
    }

    /**
     * Создать проект
     * @return 0 создано
     */
    public int createProject() throws WrongArgumentException {
        logger.trace("createProject()");
        logger.info("[CREATE PROJECT]");
        if (!this.testAuthUser())return 0;
        final String name = enterStringCommandParameter("project name");
        final String description = enterStringCommandParameter("project description");
        projectService.create(name,description,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Изменить проект
     * @param project Проект
     */
    public void updateProject(final Project project) throws WrongArgumentException{
        logger.trace("updateProject({})",project);
        final String name = enterStringCommandParameter("project name");
        final String description = enterStringCommandParameter("project description");
        projectService.update(project.getId(),name,description,project.getUserId());
        ShowResult("[OK]");
    }

    /**
     * Обновить проект по индексу
     * @return 0 обновлено
     */
    public int updateProjectByIndex() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("updateProjectByIndex()");
        logger.info("[UPDATE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = enterIntegerCommandParameter("project index")-1;
        final Project project = this.getUser().isAdmin()
                ?projectService.findByIndex(index,true)
                :projectService.findByIndex(index,this.getUser().getId(),true);
        updateProject(project);
        return 0;
    }

    /**
     * Удалить проект по имени
     * @return 0 выполнено
     */
    public int removeProjectByName() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeProjectByName()");
        logger.info("[REMOVE PROJECT BY NAME]");
        if (!this.testAuthUser())return 0;
        final String name = enterStringCommandParameter("project name");
        if (this.getUser().isAdmin()) {
            projectService.removeByName(name);
        } else {
            projectService.removeByName(name, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить проект по ID
     * @return 0 выоплнено
     */
    public int removeProjectByID() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeProjectByID()");
        logger.info("[REMOVE PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        final Long id = enterLongCommandParameter("project ID");
        if (this.getUser().isAdmin()) {
            projectService.removeById(id);
        } else {
            projectService.removeById(id, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить проект по индексу
     * @return 0 выполнено
     */
    public int removeProjectByIndex() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeProjectByIndex()");
        logger.info("[REMOVE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = enterIntegerCommandParameter("project index") -1;
        if( this.getUser().isAdmin()) {
            projectService.removeByIndex(index);
        } else {
            projectService.removeByIndex(index, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить все проекты
     * @return 0 выполнено
     */
    public int clearProject() throws WrongArgumentException {
        logger.trace("clearProject()");
        logger.info("[CLEAR PROJECT]");
        if (!this.testAuthUser()) {
            return 0;
        }
        if (this.getUser().isAdmin()) {
            projectService.clear();
        } else {
            projectService.clear(this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Вывести список проектов
     * @return 0 выполнено
     */
    public int listProject() throws WrongArgumentException {
        logger.trace("listProject()");
        logger.info("[LIST PROJECT]");
        if (!this.testAuthUser())return 0;
        int index = 1;
        for (final Project project:
            this.getUser().isAdmin()
                ?projectService.findAll()
                :projectService.findAll(this.getUser().getId())
        ) {
            logger.info("{}. {}: {}",index , project.getId(), project.getName());
            index ++;
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * ВЫвеси информацию по проекту
     * @param project Проект
     */
    public void viewProject(final Project project) {
        logger.trace("viewProject({})",project);
        logger.info("[VIEW PROJET]");
        logger.info("ID: " + project.getId());
        logger.info("NAME: " + project.getName());
        logger.info("DESCRIPTION: " + project.getDescription());
        ShowResult("[OK]");
    }

    /**
     * Вывести информацию по проекту по индексу
     * @return 0 выполнено
     */
    public int viewProjectByIndex() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("viewProjectByIndex()");
        if (!this.testAuthUser())return 0;
        final int index = enterIntegerCommandParameter("project index") -1;
        viewProject(this.getUser().isAdmin()
            ?projectService.findByIndex(index,true)
            :projectService.findByIndex(index,this.getUser().getId(),true));
        return 0;
    }
    /**
     * Поменять ИД пользователя в проекте проект искать по ИД
     * @param user Пользователь на ид которого менять
     */
    public void setProjectUserById(final User user) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("setProjectUserById({})",user);
        if (!this.testAdminUser())return;
        final Long projectId = enterLongCommandParameter("project ID");
        final Project project = projectService.findById(projectId,true);
        projectService.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        ShowResult("[OK]");
    }

    /**
     * Поменять ИД пользователя в проекте проект искать по индексу
     * @param user Пользователь на ид которого менять
     */
    public void setProjectUserByIndex(final User user) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("setProjectUserByIndex({})",user);
        if (!this.testAdminUser())return;
        final int index = enterIntegerCommandParameter("project index") -1;
        final Project project = projectService.findByIndex(index,true);
        projectService.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        ShowResult("[OK]");
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("----Project commands:");
        logger.info("project-create - Create new project by name. (pcr)");
        logger.info("project-clear - Remove all projects. (pcl)");
        logger.info("project-list - Display list of projects. (pl)");
        logger.info("project-view - View project info. (pv)");
        logger.info("project-remove-by-id - Remove project by id. (prid)");
        logger.info("project-remove-by-name - Remove project by name. (prn)");
        logger.info("project-remove-by-index - Remove project by index. (prin)");
        logger.info("project-update-by-index - Update name and description of project by index. (puin)");
        ShowResult("[OK]");
        return 0;
    }

    public int saveAsJson() throws IOException{
        projectService.saveAs(new JsonMapper(),new FileOutputStream(this.getClass().getName()+".json"));
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsXML() throws IOException {
        projectService.saveAs(new XmlMapper(),new FileOutputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }
    public int loadFromXML()  throws IOException {
        projectService.loadFrom(new XmlMapper(),new FileInputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }
    public int loadFromJSON()  throws IOException {
        projectService.loadFrom(new JsonMapper(),new FileInputStream(this.getClass().getName()+".json"));
        ShowResult("[OK]");
        return 0;
    }

}
