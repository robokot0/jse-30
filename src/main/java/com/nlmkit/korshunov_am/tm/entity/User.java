package com.nlmkit.korshunov_am.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nlmkit.korshunov_am.tm.enumerated.Role;

import java.io.Serializable;

/**
 * Пользователь
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * Идентификатор
     */
    private Long id = System.nanoTime();
    /**
     * login пользователя
     */
    private String login = "";
    /**
     * Роль пользователя
     */
    private Role role = Role.USER;
    /**
     * hash пароля пользователя
     */
    private String passwordHash = "";
    /**
     * Имя
     */
    private String firstName = "";
    /**
     * Фамилия
     */
    private String secondName = "";
    /**
     * Отчество
     */
    private String middleName = "";
    /**
     * Конструктор по умолчанию
     */
    public User(){
    }
    /**
     * Конструктор
     * @param login имя
     */
    public User(final String login) {
        this.login = login;
    }
    /**
     * Получить идентификатор
     * @return идентификатор
     */
    public Long getId() {
        return id;
    }

    /**
     * Установить идентификатор
     * @param id идентификатор
     */
    public void setId(final Long id) {
        this.id = id;
    }
    /**
     * Получить login
     * @return login
     */
    public String getLogin() {
        return login;
    }
    /**
     * Задать логин
     * @param login логин
     */
    public void setLogin(String login) {
        this.login = login;
    }
    /**
     * Получить роль
     * @return роль
     */
    public Role getRole() {
        return role;
    }
    /**
     * Задатьроль
     * @param role роль
     */
    public void setRole(Role role) {
        this.role = role;
    }
    /**
     * Получить хэш пароля
     * @return хэш пароля строка
     */
    public String getPasswordHash() {
        return this.passwordHash;
    }
    /**
     * задать hash пароля
     * @param passwordHash hash пароля
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    /**
     * Получить имя
     * @return имя
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * Получить фамилию
     * @return фамилию
     */
    public String getSecondName() {
        return secondName;
    }
    /**
     * Получить отчество
     * @return отчество
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * Присвоить имя
     * @param firstName имя
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * Присвоить фамилию
     * @param secondName фамилия
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
    /**
     * Присвоить отчество
     * @param middleName отчество
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @JsonIgnore
    public boolean isAdmin(){
        return role.equals(Role.ADMIN);
    }
}
